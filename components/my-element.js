import {LitElement, html} from 'lit-element';
import GetMyPostsDm from './get-my-posts-dm';

class MyElement extends LitElement {
  static get properties(){
    return{
      propObj :{type: Object},
      propStr : {type: String},
      users : {type: Array},
      propNum : {type: Number},

    }
  }
  constructor(){
   super();
   this.propObj = {};
   this.propStr = '';
   this.users = [];
   this.propNum = 0;

  }



  _getPosts(){
    let _dmGetPosts = new GetMyPostsDm();
    _dmGetPosts.addEventListener('success-call', this._setPosts.bind(this));
    _dmGetPosts.addEventListener('error-call', this._showModalError.bind(this));
    _dmGetPosts.generateRequest();
  }
  _showModalError(configError){
    this.errorModal = configError;
  }
  _setPosts(data){
    this.users = data.detail;
  }

  render() {
    return html`
      <div>${this.propStr}</div>
      <label>${this.propNum}</label>
      <ul>
        ${this.users.map(item =>
          html`<li>${item.name} ${item.age}</li>`
          )}
      </ul>
      <label>${this.propObj.name}</label>
      <button @click="${this._getPosts}">Click</button>
    `;
  }
}

customElements.define('my-element', MyElement);




